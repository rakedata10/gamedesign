
#include <QTcpSocket>
#include "game.h"
#include <QDataStream>
#include <QHostAddress>


Game::Game() : QObject(0), _request_id(0)
{
// connect( &_socket, SIGNAL(readyRead()), this, SLOT(readReady()) );
//_valid_header_read = false;
}

void Game::connectToServer(const QString &address, int port ) {
qDebug() << "connected to server";
_socket.abort();
_socket.connectToHost( address, port );


}

void Game::initialize() {

connect(&_socket,&QTcpSocket::connected,    this,&Game::socketConnected );
connect(&_socket,&QTcpSocket::disconnected, this,&Game::socketDisconnected );


connect(&_socket,&QTcpSocket::connected,  this,&Game::socketSendConnectRequest );
connect(&_socket,&QTcpSocket::disconnected,this,&Game::socketSendConnectRequest);

_request_id = 0;
_header_valid = false;
_client_id =0;
_game_connect_port=0;

_socket.connectToHost("127.0.0.1", 1234);

// Example code: How to create a connection request packet and send it to the server
// NB: Connection must be made first

// First, declare an instance of the packet type and let a variable hold it
gp_connect_request connect_request_packet;

// Set the necessary fields using appropriate aliases as defined in the gameprotocol.h
connect_request_packet.connect_flag = GP_CONNECT_FLAG_CONNECT;

// Attempt to send the packet to the server over the already opened connection (socket)
sendConnectRequest(connect_request_packet);

}

void Game::socketConnected() {
  connect(&_socket,&QTcpSocket::readyRead, this,&Game::socketReadReady );
  connect(&_game_socket, &QTcpSocket::readyRead, this, &Game::gameSocketReadReady);
}

void Game::socketSendConnectRequest(){


qDebug() << "Socket connected";


}

void Game::sendConnectRequest( const gp_connect_request& query ) {

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_CONNECT;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // Write body
  out.writeRawData( (char*)(&query), sizeof(gp_connect_request) );

  // "Send" block
  _socket.write( block );

}
void Game::socketDisconnected() {
    qDebug() << "SocketDisconnected()";
}

void Game::sendDSQRequest(const gp_default_server_query &query)

{
    gp_header header;
    header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
    header.type = GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY;
    header.flags.answer = 0;
    header.request_id = ++_request_id;

    // Build block
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly );
    out.setVersion( QDataStream::Qt_5_1 );

    // Write header
    out.writeRawData( (char*)(&header), sizeof(gp_header) );

    // Write body
    out.writeRawData( (char*)(&query), sizeof(gp_default_server_query) );

    // "Send" block
    _socket.write( block );
}

void Game::sendJoinRequest(const gp_join_request &request)
{
    qDebug()<<"join request";
    // Build header
    gp_header header;
    header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
    header.type = GP_REQUEST_TYPE_JOIN;
    header.flags.answer = 0;
    header.request_id = ++_request_id;

    // Build block
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly );
    out.setVersion( QDataStream::Qt_5_1 );

    // Write header
    out.writeRawData( (char*)(&header), sizeof(gp_header) );

    // Write body
    out.writeRawData( (char*)(&request), sizeof(gp_join_request) );

    // "Send" block
    _socket.write( block );
}



void Game::socketReadReady()
{
    qDebug() << "Fetching request!";
    qDebug() << " - bytes avalable:  " << _socket.bytesAvailable();

    QDataStream in(&_socket);
    in.setVersion( QDataStream::Qt_5_1 );

    // If header has not been read: fetch it
    if( !_valid_header_read ) {

      // INVARIANT: unles a valid packet of some sort is located/defined by a valid header
      //            _valid header_read is not to be set, hence it will be false.

      // Reset body size
      _body_size = 0;

      // If not enough bytes to read the header is available return to fetch more
      if( _socket.bytesAvailable() < (int)sizeof(gp_header) )
        return;

      // Read header
      in.readRawData( (char*)(&_gp_header), sizeof(gp_header) );

      // If header prefix is wrong, dismiss and return for more data
      if( _gp_header.prefix.id != _gp_header_template.id )
        return;


      // Route checking dependent on answer packet status
      if( _gp_header.flags.answer ) {

        // Get size from valid answer packet
        switch( _gp_header.type ) {
          case GP_REQUEST_TYPE_CONNECT:
            _body_size = sizeof(gp_connect_answer);
            _valid_header_read = true;
            break;

          case GP_REQUEST_TYPE_JOIN:
            _body_size = sizeof(gp_join_answer);
            _valid_header_read = true;
            break;

          case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:
            _body_size = sizeof(gp_default_server_query_answer);
            _valid_header_read = true;
            break;

          case GP_REQUEST_TYPE_ERROR:
            _body_size = sizeof(gp_client_error_response);
            _valid_header_read = true;
            break;

        default:
          return;
        }
      }
      else {

        // Get size from valid direct request packet
        switch( _gp_header.type ) {
        case GP_REQUEST_TYPE_ERROR:
          _body_size = sizeof(gp_client_error_response);
          _valid_header_read = true;
          break;

        default:
          return;
        }
      }
    }

    qDebug() << " - bytes avalable2: " << _socket.bytesAvailable();
    // Return and wait for more data if data available is less then the required body data size
    if( _socket.bytesAvailable() < _body_size )
      return;

    // Handle request
    switch( _gp_header.type ) {

      case GP_REQUEST_TYPE_ERROR:
      {
        gp_client_error_response answer;
        in.readRawData( (char*)(&answer), sizeof(gp_client_error_response) );
        handleErrorPkg( _gp_header, answer );
      } break;

      case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:
      {
        gp_default_server_query_answer answer;
        in.readRawData( (char*)(&answer), sizeof(gp_default_server_query_answer) );
        handleDSQAnswerPkg( _gp_header, answer );
      } break;

      case GP_REQUEST_TYPE_JOIN:
      {
        gp_join_answer answer;
        in.readRawData( (char*)(&answer), sizeof(gp_connect_answer) );
        handleJoinAnswerPkg( _gp_header, answer );
      } break;

      case GP_REQUEST_TYPE_CONNECT:
      {
        gp_connect_answer answer;
        in.readRawData( (char*)(&answer), sizeof(gp_connect_answer) );
        handleConnectAnswerPkg( _gp_header, answer );
      } break;

    default: break;
    }

    // Reset the valid header stat so we can look for new packages
    _valid_header_read = false;

    // If buffer data available is greater or equal to the header size, look for extra packages.
    if( _socket.bytesAvailable() >= sizeof(gp_header) )
     socketReadReady();
   }
void Game::gameSocketReadReady() {


}



void Game::handleErrorPkg(gp_header header, gp_client_error_response error)
{
    emit signHandleErrorPkg( header, error );

    if(
       _quick_connect &&
       (
         ( header.type == GP_REQUEST_TYPE_CONNECT )
         ||
         ( header.type == GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY )
         ||
         ( header.type == GP_REQUEST_TYPE_JOIN ) ) ) {

      _quick_connect = false;
         setDisconnected();
}
}
void Game::handleConnectAnswerPkg(gp_header header, gp_connect_answer answer)
{
    emit signHandleConnectAnswerPkg( header, answer );

    quint8 state = answer.state;
    if( state == GP_CONNECT_FLAG_CONNECT )
      _client_id = answer.client_id;
    else
      _client_id = 0;

    if( !_quick_connect )
      return;

    _client_id = answer.client_id;
}

void Game::handleDSQAnswerPkg(gp_header header, gp_default_server_query_answer answer)
{

qDebug() <<"handling default server query";
    if (answer.server_info.fields.connect_port)
        _game_connect_port = answer.server_info.connect_port;

    if (answer.server_info.host_name)
        strcpy(&_game_hostname[0], answer.server_info.host_name);


    emit signHandleDSQAnswerPkg(header,answer);

    gp_join_request request;

    request.client_id = _client_id;
    strcpy(&request.player_name[0], "Player1");
    request.flags.client_type = GP_JOIN_CLIENT_TYPE_PLAYER;
    request.flags.join_flag = GP_JOIN_FLAG_JOIN;
    request.team_id =0;
    strcpy(&request.class_[0],  "\0");

    sendJoinRequest(request);
}


void Game::handleJoinAnswerPkg(gp_header header, gp_join_answer answer)
{
qDebug() <<"join answer package";

    if( answer.state == GP_JOIN_FLAG_JOIN )
      _vcode = answer.validation_code;
    else
      _vcode = 0;

    if( _quick_connect )
      emit signQcHandleJoinAnswerPkg( header, answer );
    else
        emit signHandleJoinAnswerPkg( header, answer );
connectToGame();
}


void Game::sendClientVerificationAnswer(quint32 rid) {

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_CLIENT_VERIFICATION;
  header.flags.answer = 1;
  header.request_id = rid;

  // Build body
  gp_client_verification_answer body;
  body.client_id = _client_id;
  body.vcode = _vcode;

   // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // Write body
  out.writeRawData( (char*)(&body), sizeof(gp_client_verification_answer) );

  // "Send" block

    _socket.write(block);
}

void Game::testGameServerSocket() {

  qDebug() << "Testing game server socket:";

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_TEST;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // "Send" block
 _game_socket.write( block );
}
void Game::sendGameRequest(const gp_game_request &request) {

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_GAME;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // Write body
  out.writeRawData( (char*)(&request), sizeof(gp_game_request) );

  // "Send" block
  _game_socket.write( block );
}
void Game::setDisconnected()
{
    _connected = true;
    emit signDisconnected();
    emit signUIsetConnected( false );
  }


void Game::setConnected()
{
   qDebug() << "we got data from the server";
    _connected = true;
    emit signConnected();
    emit signUIsetConnected( true );

    if( _quick_connect ) {

      gp_connect_request cr;
      cr.connect_flag = GP_CONNECT_FLAG_CONNECT;
      sendConnectRequest(cr);
    }

  }
void Game::connectToGame()  {
qDebug() << "we got data from the server";

   _game_socket.connectToHost(_game_hostname, _game_connect_port);
           }


