#include "client.h"

// game protocol
#include "gameprotocol.h"

// qt
#include <QDataStream>
#include <QHostAddress>

Client::Client(QObject *parent) :
    QObject(parent)
{

  _quick_connect = false;

  _socket = new QTcpSocket(this);
  connect( _socket, SIGNAL(readyRead()), this, SLOT(readReady()) );

  _request_id = 0;
  _client_id = 0;
  _connected = false;

  _socket_game = new QTcpSocket(this);
  connect( _socket_game, SIGNAL(readyRead()), this, SLOT(readReadyGame()) );

  _gs_connected = false;
  _game_connect_port = 0;

  _valid_header_read = false;

  connect(_socket, SIGNAL(connected()), this, SLOT(setConnected()) );
  connect(_socket, SIGNAL(disconnected()), this, SLOT(setDisconnected()) );

  connect(_socket_game, SIGNAL(connected()), this, SLOT(setConnectedToGame()) );
  connect(_socket_game, SIGNAL(disconnected()), this, SLOT(setDisconnectedFromGame()) );
}

Client::~Client() {

  _socket->abort();
  delete _socket;
}

void Client::connectToGame(const QString &address, int port ) {

  _socket_game->abort();
  _socket_game->connectToHost( address, port );
}

void Client::connectToServer(const QString &address, int port ) {

  _socket->abort();
  _socket->connectToHost( address, port );
}

void Client::disconnectFromGame() {

  _socket_game->abort();
  _socket_game->disconnectFromHost();
}

void Client::disconnectFromHost() {

  _socket->abort();
  _socket->disconnectFromHost();
}

void Client::handleClientVerificationRequestPkg(gp_header header) {

  emit signHandleClientVerificationRequest( header );

  sendClientVerificationAnswer( header.request_id );

}

void Client::handleConnectAnswerPkg( gp_header header, gp_connect_answer answer ) {

  emit signHandleConnectAnswerPkg( header, answer );

  quint8 state = answer.state;
  if( state == GP_CONNECT_FLAG_CONNECT )
    _client_id = answer.client_id;
  else
    _client_id = 0;

  if( !_quick_connect )
    return;

  gp_default_server_query dsq;

  // server info
  dsq.request_flags.server_info = true;
  dsq.server_fields.connect_port = true;

  // map info
  dsq.request_flags.map_info = true;
  dsq.map_fields.width    = true;
  dsq.map_fields.height   = true;

  sendDSQRequest( dsq );
}

void Client::handleDSQAnswerPkg( gp_header header, gp_default_server_query_answer answer ) {

  if( answer.server_info.fields.connect_port )
    _game_connect_port = answer.server_info.connect_port;

  if( _quick_connect )
    emit signQcHandleDSQAnswerPkg( header, answer );
  else
    emit signHandleDSQAnswerPkg( header, answer );
}

void Client::handleErrorPkg( gp_header header, gp_client_error_response error ) {

  emit signHandleErrorPkg( header, error );

  if(
     _quick_connect &&
     (
       ( header.type == GP_REQUEST_TYPE_CONNECT )
       ||
       ( header.type == GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY )
       ||
       ( header.type == GP_REQUEST_TYPE_JOIN ) ) ) {

    _quick_connect = false;
    setDisconnected();
  }
}

void Client::handleJoinAnswerPkg(gp_header header, gp_join_answer answer) {

  if( answer.state == GP_JOIN_FLAG_JOIN )
    _vcode = answer.validation_code;
  else
    _vcode = 0;

  if( _quick_connect )
    emit signQcHandleJoinAnswerPkg( header, answer );
  else
    emit signHandleJoinAnswerPkg( header, answer );
}

void Client::handleGameUpdatePkg(gp_header header, gp_game_update update) {

  emit signHandleGameUpdatePkg( header, update );
}

void Client::joinGame() {

  _socket_game->abort();

  if( _connected )
    _socket_game->connectToHost( _socket->peerAddress(), _game_connect_port );
  else {

    qDebug() << "Cant join, not connected!";
    return;
  }


  if( _socket_game->waitForConnected( 1000 ) )
    qDebug() << _client_id << " connected to game!";
  else
    qDebug() << _client_id << " connected unsuccessfull!";
}

void Client::partGame() {

  _socket_game->abort();
  _socket_game->disconnectFromHost();
  _socket_game->waitForDisconnected( 1000 );
}

void Client::quickConnect(const QString &address, int port) {

  qDebug() << QString("Quick connect to %1:%2").arg(address).arg(port);
  _quick_connect = true;
  connectToServer( address, port );
}

void Client::readReady() {


  qDebug() << "Fetching request!";
  qDebug() << " - bytes avalable:  " << _socket->bytesAvailable();

  QDataStream in(_socket);
  in.setVersion( QDataStream::Qt_5_0 );

  // If header has not been read: fetch it
  if( !_valid_header_read ) {

    // INVARIANT: unles a valid packet of some sort is located/defined by a valid header
    //            _valid header_read is not to be set, hence it will be false.

    // Reset body size
    _body_size = 0;

    // If not enough bytes to read the header is available return to fetch more
    if( _socket->bytesAvailable() < (int)sizeof(gp_header) )
      return;

    // Read header
    in.readRawData( (char*)(&_gp_header), sizeof(gp_header) );

    // If header prefix is wrong, dismiss and return for more data
    if( _gp_header.prefix.id != _gp_header_template.id )
      return;


    // Route checking dependent on answer packet status
    if( _gp_header.flags.answer ) {

      // Get size from valid answer packet
      switch( _gp_header.type ) {
        case GP_REQUEST_TYPE_CONNECT:
          _body_size = sizeof(gp_connect_answer);
          _valid_header_read = true;
          break;

        case GP_REQUEST_TYPE_JOIN:
          _body_size = sizeof(gp_join_answer);
          _valid_header_read = true;
          break;

        case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:
          _body_size = sizeof(gp_default_server_query_answer);
          _valid_header_read = true;
          break;

        case GP_REQUEST_TYPE_ERROR:
          _body_size = sizeof(gp_client_error_response);
          _valid_header_read = true;
          break;

      default:
        return;
      }
    }
    else {

      // Get size from valid direct request packet
      switch( _gp_header.type ) {
      case GP_REQUEST_TYPE_ERROR:
        _body_size = sizeof(gp_client_error_response);
        _valid_header_read = true;
        break;

      default:
        return;
      }
    }
  }

  qDebug() << " - bytes avalable2: " << _socket->bytesAvailable();
  // Return and wait for more data if data available is less then the required body data size
  if( _socket->bytesAvailable() < _body_size )
    return;

  // Handle request
  switch( _gp_header.type ) {

    case GP_REQUEST_TYPE_ERROR:
    {
      gp_client_error_response answer;
      in.readRawData( (char*)(&answer), sizeof(gp_client_error_response) );
      handleErrorPkg( _gp_header, answer );
    } break;

    case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:
    {
      gp_default_server_query_answer answer;
      in.readRawData( (char*)(&answer), sizeof(gp_default_server_query_answer) );
      handleDSQAnswerPkg( _gp_header, answer );
    } break;

    case GP_REQUEST_TYPE_JOIN:
    {
      gp_join_answer answer;
      in.readRawData( (char*)(&answer), sizeof(gp_connect_answer) );
      handleJoinAnswerPkg( _gp_header, answer );
    } break;

    case GP_REQUEST_TYPE_CONNECT:
    {
      gp_connect_answer answer;
      in.readRawData( (char*)(&answer), sizeof(gp_connect_answer) );
      handleConnectAnswerPkg( _gp_header, answer );
    } break;

  default: break;
  }

  // Reset the valid header stat so we can look for new packages
  _valid_header_read = false;

  // If buffer data available is greater or equal to the header size, look for extra packages.
  if( _socket->bytesAvailable() >= sizeof(gp_header) )
    readReady();
}

void Client::readReadyGame() {

  qDebug() << "Fetching request!";
  qDebug() << " - bytes avalable:  " << _socket_game->bytesAvailable();

  QDataStream in(_socket_game);
  in.setVersion( QDataStream::Qt_5_0 );

  // If header has not been read: fetch it
  if( !_valid_header_read_game ) {

    // INVARIANT: unles a valid packet of some sort is located/defined by a valid header
    //            _valid header_read is not to be set, hence it will be false.

    // Reset body size
    _body_size_game = 0;

    // If not enough bytes to read the header is available return to fetch more
    if( _socket_game->bytesAvailable() < (int)sizeof(gp_header) )
      return;

    // Read header
    in.readRawData( (char*)(&_gp_header_game), sizeof(gp_header) );

    // If header prefix is wrong, dismiss and return for more data
    if( _gp_header_game.prefix.id != _gp_header_template.id )
      return;


    // Route checking dependent on answer packet status
    if( _gp_header_game.flags.answer ) {

      // Get size from valid answer packet
      switch( _gp_header_game.type ) {
        case GP_REQUEST_TYPE_GAME:
          _body_size_game = sizeof(gp_game_update);
          _valid_header_read_game = true;
          break;

        case GP_REQUEST_TYPE_ERROR:
          _body_size_game = sizeof(gp_client_error_response);
          _valid_header_read_game = true;
          break;

      default:
        return;
      }
    }
    else {

      // Get size from valid direct request packet
      switch( _gp_header_game.type ) {

        case GP_REQUEST_TYPE_CLIENT_VERIFICATION:
          _body_size_game = 0;
          _valid_header_read_game = true;
          break;

        case GP_REQUEST_TYPE_ERROR:
          _body_size_game = sizeof(gp_client_error_response);
          _valid_header_read_game = true;
          break;

      default:
        return;
      }
    }
  }

  qDebug() << " - bytes avalable2: " << _socket_game->bytesAvailable();
  // Return and wait for more data if data available is less then the required body data size
  if( _socket_game->bytesAvailable() < _body_size_game )
    return;

  // Handle request
  switch( _gp_header_game.type ) {

    case GP_REQUEST_TYPE_GAME:
    {
      gp_game_update update;
      in.readRawData( (char*)(&update), sizeof(gp_game_update) );
//      handleGameUpdatePkg( _gp_header_game, update );
      emit signHandleGameUpdatePkg( _gp_header_game, update );

    } break;

    case GP_REQUEST_TYPE_ERROR:
    {
      gp_client_error_response answer;
      in.readRawData( (char*)(&answer), sizeof(gp_client_error_response) );
      handleErrorPkg( _gp_header_game, answer );
    } break;

    case GP_REQUEST_TYPE_CLIENT_VERIFICATION:
    {
      handleClientVerificationRequestPkg( _gp_header_game );
    } break;

  default: break;
  }

  // Reset the valid header stat so we can look for new packages
  _valid_header_read_game = false;

  // If buffer data available is greater or equal to the header size, look for extra packages.
  if( _socket_game->bytesAvailable() >= sizeof(gp_header) )
    readReady();
}

void Client::sendClientVerificationAnswer(quint32 rid) {

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_CLIENT_VERIFICATION;
  header.flags.answer = 1;
  header.request_id = rid;

  // Build body
  gp_client_verification_answer body;
  body.client_id = _client_id;
  body.vcode = _vcode;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // Write body
  out.writeRawData( (char*)(&body), sizeof(gp_client_verification_answer) );

  // "Send" block
  _socket_game->write( block );
}

void Client::sendConnectRequest( const gp_connect_request& query ) {

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_CONNECT;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // Write body
  out.writeRawData( (char*)(&query), sizeof(gp_connect_request) );

  // "Send" block
  _socket->write( block );
}

void Client::sendDSQRequest(const gp_default_server_query &query) {

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out( &block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)&(header), sizeof(gp_header) );

  // Write body
  out.writeRawData( (char*)&(query), sizeof(gp_default_server_query) );

  _socket->write( block );
}

void Client::sendGameRequest(const gp_game_request &request) {

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_GAME;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // Write body
  out.writeRawData( (char*)(&request), sizeof(gp_game_request) );

  // "Send" block
  _socket_game->write( block );
}

void Client::sendJoinRequest( const gp_join_request &request ) {

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_JOIN;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // Write body
  out.writeRawData( (char*)(&request), sizeof(gp_join_request) );

  // "Send" block
  _socket->write( block );
}

void Client::sendTestPkgToServer() {

  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_TEST;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

//  qDebug() << "Client: Header test data";
//  qDebug() << " quint16 size:           " << sizeof(quint16);
//  qDebug() << " header size:            " << sizeof(header);
//  qDebug() << " gp header size:         " << sizeof(gp_header);
//  qDebug() << " gp header flag size:    " << sizeof(gp_header_flags);
//  qDebug() << " gp header prefix size:  " << sizeof(gp_header_prefix);
//  qDebug() << " p:id:                   " << header.prefix.id;
//  qDebug() << " s:                      " << header.size;
//  qDebug() << " t:                      " << header.type;
//  qDebug() << " a:                      " << header.flags.answer;
//  qDebug() << " r:                      " << header.request_id;
//  qDebug() << " ";
//  qDebug() << " type is PING:           " << ( header.type == GP_REQUEST_TYPE_PING ? "yes" : "no" );
//  qDebug() << " type is DEFAULT SERVER QUERY: " << ( header.type == GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY ? "yes" : "no" );
//  qDebug() << " type is TEST:           " << ( header.type == GP_REQUEST_TYPE_TEST ? "yes" : "no" );
//  qDebug() << " type is ERROR:          " << ( header.type == GP_REQUEST_TYPE_ERROR ? "yes" : "no" );


  QByteArray block;
  QDataStream out(&block, QIODevice::ReadWrite );
  out.setVersion( QDataStream::Qt_5_1 );

  qDebug() << "--> block.size(1): " << block.size();

//  // Write header data
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  _socket->write( block );
}

void Client::setConnected() {

  _connected = true;
  emit signConnected();
  emit signUIsetConnected( true );

  if( _quick_connect ) {

    gp_connect_request cr;
    cr.connect_flag = GP_CONNECT_FLAG_CONNECT;
    sendConnectRequest(cr);
  }
}

void Client::setConnectedToGame() {

  _gs_connected = true;
  emit signConnectedToGame();

  if( _quick_connect )
    _quick_connect = false;
}

void Client::setDisconnected() {

  _connected = true;
  emit signDisconnected();
  emit signUIsetConnected( false );
}

void Client::setDisconnectedFromGame() {

  _gs_connected = true;
  emit signDisconnectedFromGame();
}

void Client::testGameServerSocket() {

  qDebug() << "Testing game server socket:";

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_TEST;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // "Send" block
  _socket_game->write( block );
}

void Client::testNegotiationServerSocket() {

  qDebug() << "Testing negotiation server socket:";

  // Build header
  gp_header header;
  header.size = sizeof(gp_header) - sizeof(gp_header_prefix);
  header.type = GP_REQUEST_TYPE_TEST;
  header.flags.answer = 0;
  header.request_id = ++_request_id;

  // Build block
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly );
  out.setVersion( QDataStream::Qt_5_1 );

  // Write header
  out.writeRawData( (char*)(&header), sizeof(gp_header) );

  // "Send" block
  _socket->write( block );

}

