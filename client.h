#ifndef CLIENT_H
#define CLIENT_H

// qt
#include <QObject>
#include <QTcpSocket>

// game protocol
#include "gameprotocol.h"

class Client : public QObject
{
  Q_OBJECT
public:
  explicit Client(QObject *parent = 0);
  ~Client();

public slots:
  void                quickConnect( const QString& address, int port );
  void                connectToServer( const QString& address, int port );
  void                disconnectFromHost();
  void                sendTestPkgToServer();

  void                connectToGame( const QString& address, int port );
  void                disconnectFromGame();

  void                sendConnectRequest( const gp_connect_request& query );
  void                sendDSQRequest( const gp_default_server_query& query );
  void                sendJoinRequest( const gp_join_request& request );
  void                sendClientVerificationAnswer( quint32 rid );
  void                sendGameRequest( const gp_game_request& request );

  void                testNegotiationServerSocket();
  void                testGameServerSocket();

signals:
  void                signConnected();
  void                signDisconnected();

  void                signConnectedToGame();
  void                signDisconnectedFromGame();

  void                signUIsetConnected( bool state );
  void                signUIsetJoined( bool state);

  void                signHandleHeader( gp_header header, bool echo_header );
  void                signHandleErrorPkg( gp_header header, gp_client_error_response error );
  void                signHandleConnectAnswerPkg( gp_header header, gp_connect_answer answer );
  void                signHandleDSQAnswerPkg( gp_header header, gp_default_server_query_answer answer );
  void                signHandleJoinAnswerPkg( gp_header header, gp_join_answer answer );
  void                signHandleClientVerificationRequest( gp_header header);
  void                signHandleGameUpdatePkg( gp_header header, gp_game_update update );

  void                signQcHandleDSQAnswerPkg(gp_header header, gp_default_server_query_answer answer);
  void                signQcHandleJoinAnswerPkg( gp_header header, gp_join_answer answer );


private:
  gp_header_prefix    _gp_header_template;

  bool                _quick_connect;

  // Negotiation client
  QTcpSocket          *_socket;
  bool                _connected;
  quint32             _request_id;
  quint32             _client_id;

  gp_header           _gp_header;
  bool                _valid_header_read;
  quint32             _body_size;

  // Game client
  QTcpSocket          *_socket_game;
  bool                _gs_connected;
  quint16             _game_connect_port;

  gp_header           _gp_header_game;
  bool                _valid_header_read_game;
  quint32             _body_size_game;

  quint32             _vcode;


  void                joinGame();
  void                partGame();

private slots:
  void                setConnected();
  void                setDisconnected();

  void                setConnectedToGame();
  void                setDisconnectedFromGame();

  void                readReady();
  void                readReadyGame();

  void                handleErrorPkg( gp_header header, gp_client_error_response error );
  void                handleConnectAnswerPkg( gp_header header, gp_connect_answer answer );
  void                handleDSQAnswerPkg( gp_header header, gp_default_server_query_answer answer );
  void                handleJoinAnswerPkg( gp_header header, gp_join_answer answer );
  void                handleClientVerificationRequestPkg( gp_header header );
  void                handleGameUpdatePkg( gp_header header, gp_game_update update );

}; // END class Client

#endif // CLIENT_H
