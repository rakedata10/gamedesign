#include "playerdatamodel.h"



PlayerDataModel::PlayerDataTuple
PlayerDataModel::findPlayer(const QString& name) {

  auto item = std::find_if( std::begin(_data), std::end(_data),
                            [name](const Private::PlayerData& data) { return data.name == name; }
  );

  return PlayerDataTuple(item,index(item-std::begin(_data)));
}


QHash<int,QByteArray>
PlayerDataModel::roleNames() const {

  QHash<int,QByteArray> roles;
  roles[DataRoles::NameRole] = "name";
  roles[DataRoles::PositionRole] = "position";
  roles[DataRoles::IsEnemyRole] = "is_enemy";
  roles[DataRoles::ScoreRole] = "score";
  return roles;
}

int
PlayerDataModel::rowCount(const QModelIndex&) const {

  return _data.size();
}

QVariant
PlayerDataModel::data(const QModelIndex& index, int role) const {

  if( !index.isValid() ) return QVariant();

  const auto& data = _data[index.row()];
  if( role == NameRole )      return QVariant(data.name);
  if( role == PositionRole )  return QVariant(data.pos);
  if( role == ScoreRole )     return QVariant(data.score);
  if( role == IsEnemyRole )   return QVariant(data.is_enemy);

  return QVariant();
}

void PlayerDataModel::movePlayer(const QString& name, const QPointF& dir) {

    auto itr = findPlayer(name);
    if(std::get<0>(itr) == std::end(_data))
      return;

    std::get<0>(itr)->pos += dir.toPoint( );

    emit dataChanged(std::get<1>(itr),std::get<1>(itr));
}
