
#include <iostream>

#include "guiapplication.h"

#include "gameprotocol.h"


GuiApplication::GuiApplication(int& argc, char ** argv): QGuiApplication(argc, argv){
    initialize();
}
void
GuiApplication::initialize()
{

    _Window.initialize();
    connect(&_Window, SIGNAL(signconnectToServer(QString,int)), this, SLOT(connectToServer(QString,int)));
    connect(&_Window, SIGNAL (signconnectToGameserver(QString, int)), this, SLOT(connectToGameserver(QString, int)));
    connect(&_Window,SIGNAL(signmovePlayer(QString,int)),this,SIGNAL(signmovePlayer(QString,int)));


}

void
GuiApplication::connectToServer(QString hostname, int port)
{
  std::cout << "Hostname: " << hostname.toStdString() << "Port: " << port << std::endl;
  _game.connectToServer(hostname, port);

  // Now that the TCP socket is set up, try to send a connection request packet over the socket
  gp_connect_request connection_request_packet;
  connection_request_packet.connect_flag = GP_CONNECT_FLAG_CONNECT;
  _game.sendConnectRequest(connection_request_packet);
{
  gp_default_server_query dsq;

  // server info
  dsq.request_flags.server_info = true;
  dsq.server_fields.connect_port = true;

  // map info
  dsq.request_flags.map_info = true;
  dsq.map_fields.width    = true;
  dsq.map_fields.height   = true;

  _game.sendDSQRequest( dsq );

  emit _Window.signConnectedToLobby();
  }
}

void GuiApplication::connectToGameserver(QString hostname, int port)
{
    _game.connectToGame();
}




