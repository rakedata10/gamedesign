

#include "window.h"
#include <QQmlContext> //defines a context within qml engine
#include <QQuickItem> // provides basic visual item
#include <QDebug>
Window::Window(QWindow *parent) : QQuickView(parent) {

    //setMinimumSize( QSize(800, 600));
    //setResizeMode(QQuickView::SizeRootObjectToView);
    //setSource(QUrl("qrc:/resources/qml/main.qml") );


    //rootObject()->setProperty("player_name", "Raaz");
    //rootObject()->setProperty("player_score", 8);
    //connect(this, &Window::signMoveUp, this, &window::aslot);
    //connect( this, SIGNAL(signmoveup()),this, SLOT(aslot()));

//    connect (this, SIGNAL (signMoveUp()), rootObject(), SIGNAL( moveUp()));
//    connect (this, SIGNAL (signMoveDown()), rootObject(), SIGNAL( moveDown()));
//    connect (this, SIGNAL (signMoveLeft()), rootObject(), SIGNAL( moveLeft()));
//

connect (this, SIGNAL (signMoveRight()), rootObject(), SIGNAL( moveRight()));
    connect (rootObject(), SIGNAL(connectToServer(QString, int)), this, SIGNAL(connectToServer(QString, int)));
    connect (rootObject(),SIGNAL (connectToGameserver(QString, int)), this, SIGNAL(signconnectToGameserver(QString, int)));
}

void Window::initialize()

{
    _data_model.addPlayer("Raaz",QPoint(50,30),  100, false);
    _data_model.addPlayer("Secret",QPoint(165,215),160);
    _data_model.addPlayer("Raaz",QPoint(279,25), 200);

    connect(this, &QQuickView::sceneGraphInitialized, this, &Window::afterScenegraphInitialized );

    setGeometry( QRect(100,100, 800,600) );
    setResizeMode(QQuickView::SizeRootObjectToView);

    rootContext()->setContextProperty( "player_model", &_data_model);
    setSource( QUrl("qrc:/resources/qml/main.qml") );
    show();

    connect( this, SIGNAL(signConnectedToLobby()), rootObject(), SIGNAL(setConnectedToLobby()) );
//    connect (this,SIGNAL(signconnectToGame(QString,int))
}

void Window::afterScenegraphInitialized() {

  connect( rootObject(), SIGNAL(movePlayer(QString,QPointF)),
           &_data_model, SLOT(movePlayer(QString,QPointF)) );
}
//void Window::keyPressEvent(QKeyEvent *e)

//{
//    if (e->key()==Qt::Key_Up)           emit signMoveUp();
//    else if (e->key()==Qt::Key_Down)    emit signMoveDown();
//    else if (e->key()==Qt::Key_Left)    emit signMoveLeft();
//    else if (e->key()==Qt::Key_Right)   emit signMoveRight();

//    if( e->key() == Qt::Key_C ) {
//        emit signConnectedToLobby();

//    }









