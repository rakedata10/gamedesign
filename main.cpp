#include <iostream>

#include "guiapplication.h"
//units
#include "game.h"
#include "window.h"
#include "playerdatamodel.h"


int main ( int argc,char ** argv ) {

    GuiApplication a(argc, argv);
    a.initialize();
     Game c;
     c.connectToServer("127.0.0.1", 1234);
     gp_connect_request cr;
     cr.connect_flag = 1;
     c.sendConnectRequest(cr);
    return a.exec();
     return 0;
}
