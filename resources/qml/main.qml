import QtQuick 2.0
import QtQuick.Layouts 1.1

import "components"
Item
{
    signal setConnectedToGame()
    signal setConnectedToLobby()
    signal connectToServer(string hostname,int port)
    signal connectToServerclick(string hostname,int port)
    signal connectToGameserver(string hostname, int port)
    signal moveUp()
    signal moveDown()
    signal moveLeft()
    signal moveRight()

    Welcomewindow {

        id: window_welcome
        anchors.fill: parent
        Component.onCompleted: connectoserverclick.connect(connectToServer)
                }



    Lobbywindow {
        id: window_lobby
        anchors.fill: parent
        visible: false
        Component.onCompleted: connectToserverclick.connect(connectToGameserver)
    }
Gamewindow {
    id: window_game
    anchors.fill:parent
    visible:false
    Component.onCompleted: connecToserverclick.connect(connectToGame)
}
    onSetConnectedToLobby: state = "in_lobby"

    states: [
        State {
            name: "in_lobby"
            PropertyChanges { target: window_welcome; visible: false }
            PropertyChanges { target: window_lobby; visible: true }
            PropertyChanges {target:  window_game; visible:false}

}
    ]
//       onSetConnectedToGame: state = "in_game"

//        State {
//                name: "in_game"
//                PropertyChanges { target: window_welcome; visible: false }
//                PropertyChanges { target: window_lobby; visible: false }
//                PropertyChanges {target:  window_game; visible:true}


}




