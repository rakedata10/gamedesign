import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
Item {

  signal movePlayer( string player, point dir )

  Component {
    id: list_delegate

    RowLayout{
      height: 40; spacing: 30;
      Rectangle{ color: is_enemy ? "red" : "blue"; width: 40; Layout.fillHeight: true }
      Item{ Layout.fillWidth: true }
      Text{ text: name + " (" + score + ")" }
    }
  }

  Component {
    id: player_visu_delegate

    RowLayout {
      id: me
      x: position.x
      y: position.y

      Rectangle{ width: 20; height: 20; color: is_enemy ? "red" : "blue" }

      Text{ height: 20; text: name }
    }
  }

  RowLayout {
    anchors.fill: parent

    Rectangle {

      Layout.fillWidth: true
      Layout.fillHeight: true

      clip: true

      Repeater {
        anchors.fill: parent
        model: VisualDataModel {
          model: player_model
          delegate: player_visu_delegate
        }

      }

      FocusScope {
        focus: true
        Keys.onPressed: {

          if(event.key == Qt.Key_Left)
            movePlayer( "Raaz", Qt.point(-10,0) )
          else if(event.key == Qt.Key_Right)
            movePlayer( "Raaz", Qt.point(10,0) )
          else if(event.key == Qt.Key_Up)
            movePlayer( "Raaz", Qt.point(0,-10) )
          else if(event.key == Qt.Key_Down)
            movePlayer( "Raaaz", Qt.point(0,10) )
        }
      }
    }

    ListView{

      Layout.fillHeight: true
      width: 200

      model: VisualDataModel {
        model: player_model
        delegate: list_delegate
      }
    }
  }
}
