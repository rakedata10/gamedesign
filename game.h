#ifndef GAME_H
#define GAME_H
#include <QObject>
#include <QGuiApplication>
#include <QTcpSocket>
#include "gameprotocol.h"
class Game : public QObject {
    Q_OBJECT


public:
    Game();

public slots:
    void                connectToServer( const QString& address, int port );
    void                connectToGame();
    void                sendConnectRequest( const gp_connect_request& query );
    void                sendDSQRequest(const gp_default_server_query& query);
    void                sendJoinRequest(const gp_join_request &request);


    void                   initialize();
    void testGameServerSocket();

    void sendGameRequest(const gp_game_request &request);
signals:
     void                signHandleJoinAnswerPkg( gp_header header, gp_join_answer answer );
     void                signHandleConnectAnswerPkg( gp_header header, gp_connect_answer answer );
     void                signHandleDSQAnswerPkg( gp_header header, gp_default_server_query_answer answer );
     void                signQcHandleJoinAnswerPkg( gp_header header, gp_join_answer answer );
     void                signHandleErrorPkg(gp_header header, gp_client_error_response error);
     void                signConnected();
     void                signUIsetConnected(bool con);
     void                signHandleClientVerificationRequest( gp_header header);
     void                signDisconnected();
private slots:

      void                socketReadReady();
      void                gameSocketReadReady();
      void                setConnected();

      void                handleErrorPkg( gp_header header, gp_client_error_response error );
      void                handleConnectAnswerPkg( gp_header header, gp_connect_answer answer );

      void                handleDSQAnswerPkg( gp_header header, gp_default_server_query_answer answer );
      void                handleJoinAnswerPkg( gp_header header, gp_join_answer answer );
      void                setDisconnected();
      void                sendClientVerificationAnswer(quint32 rid);

      void                socketConnected();
      void                socketSendConnectRequest();
      void                socketDisconnected();

private:
     gp_header_prefix    _gp_header_template;

     QTcpSocket         _socket;
     QTcpSocket          _game_socket;
     quint32             _request_id;
     int                 _client_id;
     bool                _valid_header_read;
     quint32             _body_size;
     QTcpSocket          _socket_game;
     gp_header           _gp_header_game;
     gp_header           _gp_header;
     int                 _player_id;
     bool                 _header_valid;
     bool                _quick_connect;
     int                 _game_connect_port;
     char                _game_hostname[64];
     int                 _vcode;
     int                _connected;
};


#endif // GAME_H

