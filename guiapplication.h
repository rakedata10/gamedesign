#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H

#include "game.h"
#include "window.h"
#include "playerdatamodel.h"
//qt
#include <QGuiApplication>


class GuiApplication : public QGuiApplication {
    Q_OBJECT
public:
    explicit   GuiApplication(int& argc, char** argv);
    void       initialize();

private:
    Window     _Window;
    Game       _game;


public slots:
    void       connectToServer(QString hostname, int port);
    void       connectToGameserver(QString hostname, int port);

signals:
    void  signmovePlayer(QString id ,QPointF pos);
};


#endif // GUIAPPLICATION_H
