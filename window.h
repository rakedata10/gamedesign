#ifndef WINDOW_H
#define WINDOW_H
#include "playerdatamodel.h"
#include <QQuickView>

class Window : public QQuickView {
    Q_OBJECT
public:
    explicit Window(QWindow *parent = 0);

    void        initialize();
protected:



  //  void keyPressEvent(QKeyEvent *e)override;
private slots:
  void    afterScenegraphInitialized();

signals:

    void signConnectedToLobby();
    void signconnectToGameserver(QString, int);
    void signconnectToServer(QString, int);
    void signMoveUp();
    void signMoveDown();
    void signMoveLeft();
    void signMoveRight();
private:
  PlayerDataModel       _data_model;

};

#endif // WINDOW_H
