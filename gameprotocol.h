#ifndef GAMEPROTOCOL_H
#define GAMEPROTOCOL_H


#define GP_VERSION 0x0001


// Default data types


#ifdef _MSC_VER   // http://msdn.microsoft.com/en-us/library/b0084kay.aspx

typedef unsigned char     gp_uint8;
typedef unsigned short    gp_uint16;
typedef unsigned long     gp_uint32;
typedef unsigned __int64  gp_uint64;

typedef signed char       gp_int8;
typedef signed short      gp_int16;
typedef signed long       gp_int32;
typedef signed __int64    gp_int64;

#else

#include <stdint.h>

typedef uint8_t           gp_uint8;
typedef uint16_t          gp_uint16;
typedef uint32_t          gp_uint32;
typedef uint64_t          gp_uint64;

typedef int8_t            gp_int8;
typedef int16_t           gp_int16;
typedef int32_t           gp_int32;
typedef int64_t           gp_int64;

#endif







// Settings
#ifndef GP_TEAM_COUNT_MAX
# define GP_TEAM_COUNT_MAX 8
#endif

#ifndef GP_PLAYER_COUNT_MAX
# define GP_PLAYER_COUNT_MAX 64
#endif

#ifndef GP_RULE_COUNT_MAX
# define GP_RULE_COUNT_MAX 256
#endif

#ifndef GP_LIMIT_COUNT_MAX
# define GP_LIMIT_COUNT_MAX 4
#endif

#ifndef GP_GAME_UPDATE_COUNT_MAX
# define GP_GAME_UPDATE_COUNT_MAX GP_PLAYER_COUNT_MAX * 2
#endif

#ifndef GP_GAME_REQUEST_PARAMS_MAX
# define GP_GAME_REQUEST_PARAMS_MAX 16
#endif

#ifndef GP_MAP_COUNT_MAX
# define GP_MAP_COUNT_MAX 32
#endif

#ifndef GP_MAP_OBJECTS_COUNT_MAX
# define GP_MAP_OBJECTS_COUNT_MAX 32
#endif


#define GP_FALSE  0x0
#define GP_TRUE   0x1

#define GP_NO     0x0
#define GP_YES    0x1

#define GP_REQUEST_TYPE_PING                  0x00
#define GP_REQUEST_TYPE_CONNECT               0x01
#define GP_REQUEST_TYPE_JOIN                  0x02
#define GP_REQUEST_TYPE_CLIENT_VERIFICATION   0x03
#define GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY  0x04
#define GP_REQUEST_TYPE_GAME                  0x05
#define GP_REQUEST_TYPE_MAP                   0x06
#define GP_REQUEST_TYPE_TEST                  0xFE
#define GP_REQUEST_TYPE_ERROR                 0xFF

#define GP_CONNECT_FLAG_UNKNOWN               0x0
#define GP_CONNECT_FLAG_CONNECT               0x1
#define GP_CONNECT_FLAG_DISCONNECT            0x2

#define GP_JOIN_FLAG_UNKNOWN                  0x0
#define GP_JOIN_FLAG_JOIN                     0x1
#define GP_JOIN_FLAG_JOIN_CANCLE              0x2
#define GP_JOIN_FLAG_PART                     0x3

#define GP_JOIN_CLIENT_TYPE_PLAYER      0x0
#define GP_JOIN_CLIENT_TYPE_SPECTATOR   0x1
#define GP_JOIN_CLIENT_TYPE_BOT         0x2

#define GP_SERVER_FLAG_TYPE_UNKNOWN   0x0
#define GP_SERVER_FLAG_TYPE_LISTEN    0x1
#define GP_SERVER_FLAG_TYPE_DEDICATED 0x2
#define GP_SERVER_FLAG_TYPE_P2P       0x3


#define GP_SERVER_FLAG_OS_UNKNOWN     0x0
#define GP_SERVER_FLAG_OS_LINUX       0x1
#define GP_SERVER_FLAG_OS_WINDOWS     0x2
#define GP_SERVER_FLAG_OS_MAC         0x3

#define GP_MAP_TYPE_GP_V001   0x0
#define GP_MAP_TYPE_FILE      0x1    // cfield 01-03 is used: path, size, digest(md5)

#define GP_PLAYER_STATE_DEAD    0x0
#define GP_PLAYER_STATE_ALIVE   0x1

#define GP_PLAYER_TEAM_STATE_NOT_ASSIGNED   0x0
#define GP_PLAYER_TEAM_STATE_ASSIGNED       0x1

#define GP_LIMIT_TYPE_TIME          0x0
#define GP_LIMIT_TYPE_PLAYER_SCORE  0x1
#define GP_LIMIT_TYPE_ROUND         0x2
#define GP_LIMIT_TYPE_TEAM_SCORE    0x3

#define GP_CHAT_MESSAGE_TYPE_UNKNOWN            0x00
#define GP_CHAT_MESSAGE_TYPE_LOBBY              0x01
#define GP_CHAT_MESSAGE_TYPE_LOBBY_PRIVATE      0x02
#define GP_CHAT_MESSAGE_TYPE_INGAME             0x03
#define GP_CHAT_MESSAGE_TYPE_INGAME_TEAM        0x04
#define GP_CHAT_MESSAGE_TYPE_INGAME_PRIVATE     0x05

#define GP_GAME_OBJECT_TYPE_WORLD       0x00
#define GP_GAME_OBJECT_TYPE_PLAYER      0x01

#define GP_ERROR_UNKNOWN                      0x0000
#define GP_ERROR_QUERY_INVALID_HEADER         0x0001
#define GP_ERROR_QUERY_INVALID_TYPE           0x0002    // type in header is wrong
#define GP_ERROR_QUERY_INVALID_RECIEVER_TYPE  0x0003    // type in
#define GP_ERROR_CONNECT_UNKNOWN              0x0010
#define GP_ERROR_CONNECT_CONNECTED            0x0011
#define GP_ERROR_CONNECT_DISCONNECTED         0x0012
#define GP_ERROR_DSQ_NOT_CONNECTED            0x0020    // Not connected
#define GP_ERROR_JOIN_UNKNOWN                 0x0030
#define GP_ERROR_JOIN_SERVER_FULL             0x0031
#define GP_ERROR_JOIN_TEAM_FULL               0x0032
#define GP_ERROR_JOIN_TEAM_ID_INVALID         0x0033
#define GP_ERROR_JOIN_CLASS_INVALID           0x0034
#define GP_ERROR_JOIN_RACE_INVALID            0x0035
#define GP_ERROR_JOIN_IN_PROGRESS             0x0036
#define GP_ERROR_JOIN_NOT_IN_PROGRESS         0x0037
#define GP_ERROR_JOIN_JOINED                  0x0038
#define GP_ERROR_JOIN_NOT_JOINED              0x0039
#define GP_ERROR_CVERIFICATION_UNKNOWN        0x0040    // Client Verification
#define GP_ERROR_CVERIFICATION_INVALID_ID     0x0041
#define GP_ERROR_CVERIFICATION_INVALID_CODE   0x0042
#define GP_ERROR_GAME_REQUEST_UNKNOWN         0x0050
#define GP_ERROR_GAME_REQUEST_INVALID_CMD     0x0051
#define GP_ERROR_GAME_REQUEST_INVALID_PARAM   0x0052
#define GP_ERROR_MAP_UNKNOWN                  0x0060
#define GP_ERROR_MAP_INVALID_ID               0x0061




//namespace gp {

  #include <cstdlib>

  // Header perfix ( 64 bits - 8 bytes )
  struct __gp_header_prefix {
    gp_uint32   _reserved;
    gp_uint32   id;

    __gp_header_prefix() : _reserved(0xFFFFFFFF), id(0x04000609)  {}
  };

  typedef __gp_header_prefix gp_header_prefix;




  // Header flags (8 bit - 1 byte)
  typedef struct {

    bool      answer    : 1;
    gp_uint8  _reserved : 7;
  } gp_header_flags;

  // Header (64 bit - 8 bytes)
  typedef struct {
    gp_header_prefix  prefix;
    gp_uint16         size;         // header_size in bytes
    gp_uint8          type;         // Request type
    gp_header_flags   flags;        // Header flags
    gp_uint32         request_id;   // Request ID
  } gp_header;



  // Server flags (8 bit - 1 byte)
  typedef struct {
    gp_uint8    type              : 2;
    gp_uint8    password          : 1;
    gp_uint8    operating_system  : 2;
    gp_uint8    _reserved         : 3;
  } gp_server_flags;

  // Map Fields( 8 bit - 1 byte )
  typedef struct {
    gp_uint8    id        : 1;
    gp_uint8    name      : 1;
    gp_uint8    version   : 1;
    gp_uint8    url       : 1;
    gp_uint8    author    : 1;
    gp_uint8    width     : 1;
    gp_uint8    height    : 1;
    gp_uint8    _reserved : 1;
  } gp_map_info_t;



  // Map Info()
  typedef struct {
    gp_map_info_t   fields;
    gp_uint32       id;
    char            name[64];
    char            version[32];
    char            url[256];
    char            author[32];
    double          width;
    double          height;
  } gp_map_info;



  // Server Info flags ( 16 bits - 2 bytes )
  typedef struct {
    gp_uint8          game_name       : 1;
    gp_uint8          server_flags    : 1;
    gp_uint8          host_name       : 1;
    gp_uint8          connect_port    : 1;
    gp_uint8          game_type       : 1;
    gp_uint8          game_state      : 1;
    gp_uint8          _reserved1      : 2;

    gp_uint8          max_slots       : 1;
    gp_uint8          reserved_slots  : 1;
    gp_uint8          player_count    : 1;
    gp_uint8          bot_count       : 1;
    gp_uint8          _reserved2      : 4;

  } gp_server_info_t;


  // Server Info ()
  typedef struct {
    gp_server_info_t  fields;
    char              game_name[64];
    gp_server_flags   flags;
    char              host_name[64];
    gp_uint16         connect_port;

    char              game_type[32];
    char              game_state[32];

    gp_uint32         max_slots;
    gp_uint32         player_count;
    gp_uint32         bot_count;
    gp_uint32         reserved_slots;
  } gp_server_info;


  // Team info fields ( 8 bits - 1 byte )
  typedef struct {
    gp_uint8  name          : 1;
    gp_uint8  id            : 1;
    gp_uint8  score         : 1;
    gp_uint8  avg_ping      : 1;
    gp_uint8  avg_loss      : 1;
    gp_uint8  member_count  : 1;
    gp_uint8  _reserved     : 2;
  } gp_team_info_t;

  // Team info ( 608 bits - bytes 76 )
  typedef struct {
    char        name[64];
    gp_uint32     id;

    gp_uint32   score;

    gp_uint16   avg_ping;
    gp_uint16   avg_loss;

    gp_uint32   member_count;
  } gp_team_info;

  // Team list ( varsize )
  typedef struct {
    gp_uint32         count;
    gp_team_info_t    info_fields;
    gp_team_info      list[GP_TEAM_COUNT_MAX];

  } gp_team_list;



  // Player flags ( 8 bits - 1 byte )
  typedef struct {
    gp_uint8    bot         : 1;  // is bot
    gp_uint8    state       : 2;  // dead/alive (state)
    gp_uint8    team_state  : 2;  // assigned/not assigned
    gp_uint8    _reserved   : 3;
  } gp_player_flags;



  // Player info fields ( 24 bits - 3 bytes )
  typedef struct {
    gp_uint8    name        : 1;
    gp_uint8    flags       : 1;
    gp_uint8    id          : 1;
    gp_uint8    slot        : 1;
    gp_uint8    team_id     : 1;
    gp_uint8    race        : 1;
    gp_uint8    class_      : 1;
    gp_uint8    _reserved1  : 1;

    gp_uint8    score       : 1;
    gp_uint8    frags       : 1;
    gp_uint8    kills       : 1;
    gp_uint8    deaths      : 1;
    gp_uint8    suicides    : 1;
    gp_uint8    team_kills  : 1;
    gp_uint8    _reserved2  : 2;

    gp_uint8    ping        : 1;
    gp_uint8    loss        : 1;
    gp_uint8    model       : 1;
    gp_uint8    time        : 1;
    gp_uint8    _reserved3  : 4;

  } gp_player_info_t;

  // Player info ( bits - bytes )
  typedef struct {

    char              name[64];
    gp_player_flags   flags;

    gp_uint32         id;
    gp_uint32         slot;

    gp_uint32         team_id;
    char              race[32];
    char              class_[32];

    gp_int32           score;
    gp_uint32         frags;
    gp_uint32         kills;
    gp_uint32         deaths;
    gp_uint32         suicides;
    gp_uint32         team_kills;

    gp_uint16         ping;
    gp_uint16         loss;

    char              model[32];
    gp_uint16         time;
  } gp_player_info;

  // Player list
  typedef struct {
    gp_uint32         count;
    gp_player_info_t  info_fields;
    gp_player_info    list[GP_PLAYER_COUNT_MAX];
  } gp_player_list;


  // Rule ( 2560 bits - 320 bytes )
  typedef struct {
    char    key[64];
    char    value[256];
  } gp_rule;

  // Rule list ( varsize )
  typedef struct {
    gp_uint32   count;
    gp_rule     rules[GP_RULE_COUNT_MAX];
  } gp_rule_list;


  // Limit flags( 8 bits - 1 byte )
  typedef struct {
    gp_uint8    limit_left : 1; // whether there is any remaining
    gp_uint8    type       : 4; // ex: time, bullets
    gp_uint8    _reserved  : 3;
  } gp_limit_flags;

  // Limit (
  typedef struct {
    gp_limit_flags  flags;
    gp_uint32       limit;      // the actual limit
    gp_uint32       limit_left; // the actual remaining
  } gp_limit;

  // Limit list ( varsize )
  typedef struct {
    gp_uint32       count;
    gp_limit        limits[GP_LIMIT_COUNT_MAX];
  } gp_limit_list;

  // Connect query
  typedef struct {
    gp_uint8  connect_flag : 4;
    gp_uint8  _reserved    : 4;
  } gp_connect_request;


  // Connect query answer
  typedef struct {
    gp_uint8    state;
    gp_uint32   client_id;
  } gp_connect_answer;

  // Join flags
  typedef struct {
    gp_uint8  join_flag   : 2;
    gp_uint8  client_type : 4;
    gp_uint8  _reserved   : 2;
  } gp_join_request_flags;

  // Join request
  typedef struct {
    gp_uint32               client_id;
    gp_join_request_flags   flags;
    char                    player_name[64];
    gp_uint32               team_id;
    char                    race[32];
    char                    class_[32];
  } gp_join_request;

  // Join answervoid                sendJoinRequest(const gp_join_request &request);
  typedef struct {
    gp_uint8    state       : 1;
    gp_uint8    _reserved   : 7;

    gp_uint32   validation_code;
  } gp_join_answer;

  // Client error response
  typedef struct {
    gp_uint16     error;
  } gp_client_error_response;

  // Client verification answer
  typedef struct {
    gp_uint32     client_id;
    gp_uint32     vcode;
  } gp_client_verification_answer;

  // Default query request flags ( 8 bits - 1 byte )
  typedef struct {
    gp_uint8          server_info : 1;
    gp_uint8          map_info    : 1;
    gp_uint8          team_list   : 1;
    gp_uint8          player_list : 1;
    gp_uint8          rule_list   : 1;
    gp_uint8          limit_list  : 1;
    gp_uint8          _reserved   : 2;
  } gp_default_server_query_flags_t;


  // Default server query ( bits - bytes )
  typedef struct {
    gp_default_server_query_flags_t   request_flags;
    gp_server_info_t                  server_fields;
    gp_map_info_t                     map_fields;
    gp_team_info_t                    team_fields;
    gp_player_info_t                  player_fields;
  } gp_default_server_query;


  // Default server answer ( bits - bytes )
  typedef struct {
    gp_default_server_query_flags_t     request_flags;
    gp_server_info                      server_info;
    gp_map_info                         map_info;
    gp_team_list                        team_list;
    gp_player_list                      player_list;
    gp_rule_list                        rule_list;
    gp_limit_list                       limit_list;
  } gp_default_server_query_answer;

  typedef struct {
    gp_uint8      type;
    gp_uint32     sender_id;
    gp_uint32     reciever_id;    // Team/player/etc ID
    char          message[4096];
  } gp_chat_message;


  /////////////////// Default Game query


  typedef struct {
    gp_uint32         count;
    gp_map_info_t     info_fields;
    gp_map_info       list[GP_MAP_COUNT_MAX];
  } gp_map_list;


  // Default query request flags ( 8 bits - 1 byte )
  typedef struct {
    gp_uint8          map_list    : 1;
    gp_uint8          cmap_id     : 1;    // current map id
    gp_uint8          _reserved   : 6;
  } gp_default_game_query_flags_t;



  typedef struct {
    gp_default_game_query_flags_t   request_flags;
  } gp_default_game_query;

  typedef struct {
    gp_default_game_query_flags_t   request_flags;
    gp_map_list                     map_list;
    gp_uint32                       cmap_id;
  } gp_default_game_query_answer;



  ////////////////// GAME RELATED STUFFIX

  // homogenous matrix (2d)
  // ordered by rows
  //
  // 1 0 | 0 [0 - 2]
  // 0 1 | 0 [3 - 5]
  // - -   -
  // 0 0 | 1 [6 - 8]

  template <typename T, int n>
  struct gp_hqmatrix__ {

    T m[(n+1)*(n+1)];
  };

  template <typename T>
  struct gp_hqmatrix_2__ : gp_hqmatrix__<T,2> {

    const T&    getM11() const { return this->m[0]; }
    const T&    getM21() const { return this->m[1]; }
    const T&    getM31() const { return this->m[2]; }
    const T&    getM12() const { return this->m[3]; }
    const T&    getM22() const { return this->m[4]; }
    const T&    getM32() const { return this->m[5]; }
    const T&    getM13() const { return this->m[6]; }
    const T&    getM23() const { return this->m[7]; }
    const T&    getM33() const { return this->m[8]; }

    void        setM11( const T& t ) { this->m[0] = t; }
    void        setM21( const T& t ) { this->m[1] = t; }
    void        setM31( const T& t ) { this->m[2] = t; }
    void        setM12( const T& t ) { this->m[3] = t; }
    void        setM22( const T& t ) { this->m[4] = t; }
    void        setM32( const T& t ) { this->m[5] = t; }
    void        setM13( const T& t ) { this->m[6] = t; }
    void        setM23( const T& t ) { this->m[7] = t; }
    void        setM33( const T& t ) { this->m[8] = t; }
  };

  typedef gp_hqmatrix_2__<int>    gp_hqmatrix_2i;
  typedef gp_hqmatrix_2__<float>  gp_hqmatrix_2f;


  typedef struct {
    char        param[16];
  } gp_game_request_param;

  typedef struct {
    char                    cmd[16];
    gp_uint32               param_count;
    gp_game_request_param   params[GP_GAME_REQUEST_PARAMS_MAX];
  } gp_game_request;


  typedef struct {
    gp_uint8          obj_type;
    gp_uint32         id;
    char              type[16];
    gp_hqmatrix_2f    matrix;
  } gp_game_object;

  typedef struct {
    gp_uint32         count;
    gp_game_object    list[GP_GAME_UPDATE_COUNT_MAX];
  } gp_game_update;


  typedef struct {
    gp_uint32         id;
  } gp_map_query;

  typedef struct {
    gp_uint32         id;
    char              type[16];
    double            height;
    double            width;
    gp_hqmatrix_2f    matrix;
  } gp_map_obj_v001;


  typedef struct {
    gp_uint32         count;
    gp_map_obj_v001   list[GP_MAP_OBJECTS_COUNT_MAX];
  } gp_map_v001;


//} // END namespace



#endif // GAMEPROTOCOL_H
