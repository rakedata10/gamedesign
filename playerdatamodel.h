#ifndef PLAYERDATAMODEL_H
#define PLAYERDATAMODEL_H


// qt
#include <QPoint>
#include <QAbstractListModel>
#include <QDebug>
#include <QtPlugin>
//stl
#include<algorithm>



namespace Private {

  struct PlayerData {
    explicit PlayerData() : name{}, pos{}, is_enemy{}, score{} {}

    explicit PlayerData(const QString& name_in, const QPoint& pos_in,
                        int score_in, bool is_enemy_in)
    : name{name_in}, pos{pos_in}, is_enemy{is_enemy_in}, score{score_in} {}

    QString   name;
    QPoint    pos;
    bool      is_enemy;
    int       score;
  }; // END PlayerData struct

}


class PlayerDataModel : public QAbstractListModel {
  Q_OBJECT
public:
  enum DataRoles : int {
    NameRole = Qt::UserRole + 1,
    PositionRole,
    ScoreRole,
    IsEnemyRole
  };

  using QAbstractListModel::QAbstractListModel;

  void                            addPlayer( const QString& name, const QPoint& pos, int score, bool is_enemy = true) {

      beginInsertRows(QModelIndex(),rowCount(),rowCount());
      _data.append(Private::PlayerData(name,pos,score,is_enemy));
      endInsertRows();
  }

  QHash<int, QByteArray>          roleNames() const override;
  int                             rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant                        data(const QModelIndex &index, int role) const override;

public slots:
  void                            movePlayer( const QString& name, const QPointF& dir );

private:
  using PlayerDataTuple = std::tuple<QVector<Private::PlayerData>::Iterator,QModelIndex>;

  PlayerDataTuple                 findPlayer( const QString& name );

  QVector<Private::PlayerData>    _data;

}; // END class PlayerDataModel


#endif // PLAYERDATAMODEL_H
